<?php
function annotations_admin_settings_form($form, &$form_state) {
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $options[$node_type->type] = $node_type->name;
  }
  $form['annotations_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Users may annotate these content types'),
    '#options' => $options,
    '#default_value' => variable_get('annotations_node_types', array()),
    '#description' => t('A textarea will be shown on these content types to make user-specific annotations.'),
  );
  $form['annotations_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  return $form;
}

function annotations_admin_settings_form_submit($form, &$form_state) {
  variable_set('annotations_node_types', $form_state['values']['annotations_node_types']);
  field_cache_clear();
}
